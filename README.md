# Friendly Sports Betting

Dapp contracts for making friendly (low stakes) bets on sporting events. 
Written in Vyper, integrates with Custom Oracle (https://gitlab.com/JamesHutchison/custom-oracle)

# Contracts to have

- Sports pool grid-iron football squares
  - 10x10 grid where you buy a square, which contributes to the pool. At
    the end of each quarter, the score in the 1's column is compared
    to find the winner for a given row/column on the grid. The number assignment
    for each row/column are randomly assigned after all squares have been purchased. 
  - For example, you buy 3 squares in a single row or column for 1 TRX each. After the random assignment,
    you have Seattle 3 - Dallas 0, Seattle 5 - Dallas 0, and Seattle 1 - Dallas 0.
  - For example, if the score was Seattle 7 - Dallas 10 at the end of the first quarter, 
    then the person who bought the Seattle 7 - Dallas 0 square will win a quarter of the pot.
- Faux stock market
  - Buy and sell shares, the pot is divided up among the winning shares.
  - I've developed some unique components that allow for fair trading with the high latencies
    found in block chain interaction. Details intentionally omitted because I'm contemplating
    spending money to file a patent for it.
- And more!
  
# Language

- Vyper for smart contracts (https://github.com/ethereum/vyper)
- Web portal written in Python

# Cryptos Supported

- TRX
- ETH
- etc

# Architecture

- TBD, but language will be Python. Most likely front-end will be app engine standard and
backend tasks will be app engine flex with a docker container that can interface with the blockchain.
- Datastore used for saving
- Unclear if a metamask equivalent will be available in time for TRX integration. Could
  create private wallets and users could transfer in TRX and interact that way. When done, they
  can transfer out.

# Can money be made?

I think it makes sense to charge a trivial fee to cover hosting costs and to help compensate
developers for taking their time to make this possible. We could also scrounge together a bounty
system to help expedite work on the project.

Perhaps a TRX token can be generated and devs can earn TRX from the pool by getting rewarded with tokens.

Players could tip the developers, which would then be split among contributors.

# How can we prevent copy cats from stealing the project if it succeeds?

TBD. Given the borderless nature of cryptos this is pretty hard. Even looking at Tronix, its done a lot of
hard forking as a means of getting to where it is today, which means someone else did the hard work and they reap the
benefit. It's possible after getting a team of devs together we may decide to take this closed source to protect
our IP.

The faux stock market idea that I have is pretty unique. It has some unique components
that I haven't seen before and don't seem to be patented, so it could be patented.